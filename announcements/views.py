# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.

from django.http import HttpResponse
from django.http import HttpResponseRedirect

from . models import Announcement
from . forms import add_announcement, edit_announcement
from users.models import User, Staff
from django.contrib.auth.decorators import login_required

@login_required
def index(request):
    announcements = Announcement.objects.all()
    return render(request, "announcements/list.html", {"title": "Объявления", "announcements": sorted(announcements, key = lambda announcement: announcement.date_pub, reverse = True)})

@login_required
def add(request):
    if request.method == 'POST':
        form = add_announcement(request.POST)
        if form.is_valid():
            announcement = Announcement()
            announcement.title = request.POST.get("title")
            announcement.body = request.POST.get("body")
            # author = Staff.objects.get(id = request.POST.get("author"))
            announcement.author = request.user
            announcement.save()
            return HttpResponseRedirect("/announcements/")
    else:
        form = add_announcement()
        return render(request, "announcements/add.html", {"title": "Добавить объявление", "form": form})

@login_required
def open_edit(request):
    if request.method == 'POST':
        announcement = Announcement.objects.get(id = request.POST.get("id"))
        author = Staff.objects.get(user = announcement.author)
        announcement = {'id': announcement.id, 'title': announcement.title, 'body': announcement.body}
        form = edit_announcement(announcement)
        return render(request, "announcements/edit.html", {"title": "Изменить объявление", "form": form, 'author': author})

@login_required
def edit(request):
    if request.method == 'POST':
        form = add_announcement(request.POST)
        if form.is_valid():
            announcement = Announcement.objects.get(id = request.POST.get("id"))
            announcement.title = request.POST.get("title")
            announcement.body = request.POST.get("body")
            author = Staff.objects.get(id = request.POST.get("author"))
            announcement.author = author.user
            announcement.save()
            return HttpResponseRedirect("/announcements/")

@login_required
def delete(request):
    if request.method == 'POST':
        id = request.POST.get("id")
        Announcement.objects.filter(id = id).delete()
        return HttpResponseRedirect("/announcements/")

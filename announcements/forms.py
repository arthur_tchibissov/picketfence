# -*- coding: utf-8 -*-
from django import forms

from users.models import Staff

class add_announcement(forms.Form):
    title = forms.CharField(label = 'Тема', widget=forms.TextInput(attrs={'size': 60, 'style': 'width: 400px'}))
    body = forms.CharField(widget = forms.Textarea(attrs={'cols': 100, 'rows': 40}), label = 'Сообщение')

class edit_announcement(forms.Form):
    id = forms.CharField(widget = forms.HiddenInput)
    title = forms.CharField(label = 'Тема', widget=forms.TextInput(attrs={'size': 60, 'style': 'width: 400px'}))
    body = forms.CharField(widget = forms.Textarea(attrs={'cols': 100, 'rows': 40}), label = 'Сообщение')

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.

from . models import User, Student, Staff, Parent, schForm


admin.site.register(Student)
admin.site.register(Staff)
admin.site.register(Parent)
admin.site.register(schForm)

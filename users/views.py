# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from operator import attrgetter

from . models import Staff, Student, Parent, schForm
from django.contrib.auth.models import User
from . forms import UpdateUserForm, CreateUserForm, edit_staff, importForm, chForm

from django.http import HttpResponse
from django.http import HttpResponseRedirect

import csv, datetime #По-лучше изучить эти библиотеки!
import django.utils.encoding
from django.contrib.auth.decorators import login_required
# Create your views here.

@login_required
def index(request):
    return render(request, "users/index.html", {"title": "Пользователи"})

@login_required
def list(request, type):
    if type == 'staff':
        users = Staff.objects.all()
        title = 'Сотрудники'
    elif type == 'students':
        users = Student.objects.all()
        title = 'Ученики'
    elif type == 'parents':
        users = Parent.objects.all()
        title = 'Родители'
    q = users.count()
    #for user in users:
    #    if user.user.sex == 'm':
    #        user.user.sex = "М"
    #    elif user.user.sex == 'f':
    #        user.user.sex = 'Ж'
    return render(request, "users/list.html", {"type": type,
                                               "title": title,
                                               "users": sorted(users, key = attrgetter('user.last_name', 'user.first_name')),
                                               "q": q})
@login_required
def delete(request, type):
    if request.method == 'POST':
        id = request.POST.get("id")
        if type == 'staff':
            users = Staff.objects.get(user__id = id).delete()
            title = 'Сотрудники'
        elif type == 'students':
            users = Student.objects.get(user__id = id).delete()
            title = 'Ученики'
        elif type == 'parents':
            users = Parent.objects.get(user__id = id).delete()
            title = 'Родители'
        return HttpResponseRedirect("/users/" + type)

@login_required
def CreateOrUpdateUser(request, type):
    if request.method == 'POST':
        id = request.POST.get('id')
        user = User.objects.get(id = id)
        chF = ''
        if (type == 'student'):
            student = Student.objects.get(user=user)
            c = {'form': schForm.objects.get(id = student.schForm.id)}
            chF = chForm(c)

        user = {
                'surname': user.last_name,
                'firstname': user.first_name,
                #'patronymic': user.patronymic,
                #'sex': user.sex,
                #'phone_number': user.phone_number,
                'email': user.email,
                'username': user.username,
                #'password': user.password,
                #'date_birth': user.date_birth
                }
        form = UpdateUserForm(user)
        if type == 'staff':
            title = 'Изменить данные сотрудника'
        elif type == 'students':
            title = 'Изменить данные ученика'
        elif type == 'parents':
            title = 'Изменить данные родителя'
        return render(request, "users/createOrUpdateUser.html", {"type": type, "title": title, "form": form, 'chF': chF, "id": id})
    elif request.method == 'GET':
        id = ''
        form = CreateUserForm()
        chF = chForm()
        if type == 'staff':
            title = 'Добавить сотрудника'
        elif type == 'students':
            title = 'Добавить ученика'
        elif type == 'parents':
            title = 'Добавить родителя'
        return render(request, "users/createOrUpdateUser.html", {"type": type, "title": title, "form": form, 'chF': chF, "id": id})

@login_required
def CreatingOrUpdatingUser(request, type):
    if request.method == 'POST':
        id = request.POST.get("id")
        if id == '':
            form = CreateUserForm(request.POST)
            if form.is_valid():
                user = User.objects.create_user(request.POST.get("username"), request.POST.get("email"), request.POST.get("password"))
                user.last_name = request.POST.get("surname")
                user.first_name = request.POST.get("firstname")
                    #user.patronymic = request.POST.get("patronymic")
                    #user.sex = request.POST.get("sex")
                    #user.phone_number = request.POST.get("phone_number")
                    #user.email = request.POST.get("email")
                    #user.username = request.POST.get("username")
                    #user.password = request.POST.get("password")
                    #user.date_birth = request.POST.get("date_birth")
                user.save()
                if type == 'staff':
                    typedUser = Staff()
                    title = 'Сотрудники'
                elif type == 'students':
                    typedUser = Student()
                    typedUser.schForm = schForm.objects.get(id = request.POST.get("form"))
                    title = 'Ученики'
                elif type == 'parents':
                    typedUser = Parent()
                    title = 'Родители'
                typedUser.user = user
                typedUser.save()
        else:
            form = UpdateUserForm(request.POST)
            if form.is_valid():
                user = User.objects.get(id = request.POST.get("id"))
                user.last_name = request.POST.get("surname")
                user.first_name = request.POST.get("firstname")
                #user.patronymic = request.POST.get("patronymic")
                #user.sex = request.POST.get("sex")
                #user.phone_number = request.POST.get("phone_number")
                user.email = request.POST.get("email")
                user.username = request.POST.get("username")
                if 'form' in request.POST:
                    student = Student.objects.get(user = user)
                    student.schForm = schForm.objects.get(id = request.POST.get("form"))
                    student.save()
                #user.password = request.POST.get("password")
                #user.date_birth = request.POST.get("date_birth")
                user.save()
        return HttpResponseRedirect("/users/" + type)

@login_required
def importing(request, type):
    if request.method == 'POST':
        form = importForm(request.POST, request.FILES)
        if form.is_valid():
            file = request.FILES['file']
            reader = csv.DictReader(file, delimiter=str(u',').encode('utf-8'), fieldnames = ['surname', 'firstname', 'patronymic', 'sex', 'phone_number', 'email', 'username', 'password', 'date_birth', 'year', 'number'])
            i = 1
            for row in reader:
                if i == 1:
                    i = i + 1;
                    continue
                else:
                    user = User.objects.create_user(row['username'], row['email'], row['password'])
                    user.last_name = row['surname']
                    user.first_name = row['firstname']
                        #user.patronymic = request.POST.get("patronymic")
                        #user.sex = request.POST.get("sex")
                        #user.phone_number = request.POST.get("phone_number")
                        #user.email = request.POST.get("email")
                        #user.username = request.POST.get("username")
                        #user.password = request.POST.get("password")
                        #user.date_birth = request.POST.get("date_birth")
                    #try:
                    #    user.date_birth = datetime.datetime.strptime(row['date_birth'], '%Y-%m-%d').date()
                    #except:
                    #    continue
                    user.save()
                    if type == 'staff':
                        typedUser = Staff()
                    elif type == 'students':
                        typedUser = Student()
                        f = schForm.objects.get(year = row['year'], number = row['number'])
                        typedUser.schForm = f
                    elif type == 'parents':
                        typedUser = Parent()
                    typedUser.user = user
                    typedUser.save()
            return HttpResponseRedirect("/users/" + type)
    else:
        form = importForm()
        return render(request, "users/import.html", {"title": "Импорт данных сотрудников", "form": form})

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from django.contrib.auth.models import User

# Create your models here.

    #def __str__(self):
        #return u'{}'.format(self.title)

class Staff(models.Model):
    user = models.ForeignKey(User, on_delete = models.DO_NOTHING)
    class Meta:
        verbose_name = (u'Сотрудник')
        verbose_name_plural = (u'Сотрудники')

    def __unicode__(self):
        return u"%s" % self.user.last_name + " " + self.user.first_name

    def __str__(self):
        return self.__unicode__()
#    def __str__(self):
    #    return u'{}'.format(self.user)

class Parent(models.Model):
    user = models.ForeignKey(User, on_delete = models.DO_NOTHING)
    class Meta:
        verbose_name = (u'Родитель')
        verbose_name_plural = (u'Родители')

    def __unicode__(self):
        return u"%s" % self.user.last_name + " " + self.user.first_name
    def __str__(self):
        return self.__unicode__()

class schForm(models.Model):
    year = models.IntegerField()
    number = models.IntegerField()
    head = models.ForeignKey(Staff, on_delete = models.DO_NOTHING)
    class Meta:
        verbose_name = (u'Класс')
        verbose_name_plural = (u'Классы')

    def __unicode__(self):
        return u"%s" % str(self.year) + "-" + str(self.number)
    def __str__(self):
        return self.__unicode__()

class Student(models.Model):
    user = models.ForeignKey(User, on_delete = models.DO_NOTHING)
    schForm = models.ForeignKey(schForm, on_delete = models.DO_NOTHING)
    class Meta:
        verbose_name = (u'Ученик')
        verbose_name_plural = (u'Ученики')
    def __unicode__(self):
        return u"%s" % self.user.last_name + " " + self.user.first_name

    def __str__(self):
        return self.__unicode__()

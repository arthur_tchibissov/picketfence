"""project10 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from . import views
#form django.urls import path

urlpatterns = [
    url(r'^$', views.index),
    #staff
    url(r'^staff$', views.list, {'type': 'staff'}),
    url(r'^staff/delete$', views.delete, {'type': 'staff'}),
    url(r'^staff/add$', views.CreateOrUpdateUser, {'type': 'staff'}),
    url(r'^staff/edit$', views.CreateOrUpdateUser, {'type': 'staff'}),
    url(r'^staff/editing$', views.CreatingOrUpdatingUser, {'type': 'staff'}),
    url(r'^staff/import$', views.importing, {'type': 'staff'}),
    #students
    url(r'^students$', views.list, {'type': 'students'}),
    url(r'^students/delete$', views.delete, {'type': 'students'}),
    url(r'^students/add$', views.CreateOrUpdateUser, {'type': 'students'}),
    url(r'^students/edit$', views.CreateOrUpdateUser, {'type': 'students'}),
    url(r'^students/editing$', views.CreatingOrUpdatingUser, {'type': 'students'}),
    url(r'^students/import$', views.importing, {'type': 'students'}),
    #parents
    url(r'^parents$', views.list, {'type': 'parents'}),
    url(r'^parents/delete$', views.delete, {'type': 'parents'}),
    url(r'^parents/add$', views.CreateOrUpdateUser, {'type': 'parents'}),
    url(r'^parents/edit$', views.CreateOrUpdateUser, {'type': 'parents'}),
    url(r'^parents/editing$', views.CreatingOrUpdatingUser, {'type': 'parents'}),
    url(r'^parents/import$', views.importing, {'type': 'parents'}),
]

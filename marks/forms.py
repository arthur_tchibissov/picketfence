# -*- coding: utf-8 -*-
from django import forms

from .models import Markbook, WorkType

class MarkbookForm(forms.Form):
    markbook = forms.ModelChoiceField(queryset = Markbook.objects.all(), widget = forms.Select(attrs={'onchange': "showMarkbook();"}))

class NewMarkbookForm(forms.Form):
    date = forms.DateField()
    title = forms.CharField(label = 'Тема урока', widget = forms.TextInput(attrs={'size': 60}))

class LessonForm(forms.Form):
    # date = forms.DateField(label = 'Дата урока', widget = forms.SelectDateWidget())
    topic = forms.CharField(label = 'Тема урока', widget = forms.TextInput(attrs={'size': 60}), required = False)
    hometask = forms.CharField(label = 'Домашнее задание', widget = forms.TextInput(attrs={'size': 60}), required = False)
    type = forms.ModelChoiceField(queryset = WorkType.objects.all(), empty_label = None, to_field_name = 'id', widget = forms.Select())

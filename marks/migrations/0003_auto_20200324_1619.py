# -*- coding: utf-8 -*-
# Generated by Django 1.11.27 on 2020-03-24 16:19
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('marks', '0002_auto_20200324_1556'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='markbook',
            options={'verbose_name': '\u0416\u0443\u0440\u043d\u0430\u043b', 'verbose_name_plural': '\u0416\u0443\u0440\u043d\u0430\u043b\u044b'},
        ),
        migrations.AlterModelOptions(
            name='period',
            options={'verbose_name': '\u041f\u0435\u0440\u0438\u043e\u0434', 'verbose_name_plural': '\u041f\u0435\u0440\u0438\u043e\u0434\u044b'},
        ),
        migrations.AlterModelOptions(
            name='subject',
            options={'verbose_name': '\u041f\u0440\u0435\u0434\u043c\u0435\u0442', 'verbose_name_plural': '\u041f\u0440\u0435\u0434\u043c\u0435\u0442\u044b'},
        ),
    ]

# -*- coding: utf-8 -*-
# Generated by Django 1.11.27 on 2020-03-24 18:14
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0005_auto_20200324_1619'),
        ('marks', '0005_auto_20200324_1813'),
    ]

    operations = [
        migrations.CreateModel(
            name='Hometask',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=150)),
                ('lesson', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='marks.Lesson')),
            ],
        ),
        migrations.CreateModel(
            name='HometaskMark',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('hometask', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='marks.Hometask')),
                ('student', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='users.Student')),
            ],
        ),
        migrations.CreateModel(
            name='Work',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=150)),
                ('lesson', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='marks.Lesson')),
            ],
        ),
        migrations.CreateModel(
            name='WorkMark',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('hometask', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='marks.Hometask')),
                ('student', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='users.Student')),
            ],
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from .forms import MarkbookForm, LessonForm

from .models import Markbook, Lesson, WorkMark, Work, Result, WorkType, Absence
from users.models import Student

from operator import attrgetter

from collections import OrderedDict

from django.http import HttpResponseRedirect

from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def index(request, markbookId = ''):
    if markbookId == '0':
        try:
            del request.session['openedMarkbook']
            return HttpResponseRedirect("/markbook/")
        except:
            pass
    if markbookId == '':
        if 'openedMarkbook' in request.session:
            return HttpResponseRedirect("/markbook/" + str(request.session['openedMarkbook']) + "/")
        else:
            MarkbookChoice = MarkbookForm()
            return render(request, "marks/index.html", {"title": "Классный журнал", "MarkbookChoice": MarkbookChoice, "error": True})
    else:
        request.session['openedMarkbook'] = markbookId
        markbook = Markbook.objects.get(id = int(markbookId))
        MarkbookChoice = MarkbookForm({'markbook': markbook})
    # if ((request.method == 'POST') or ('openedMarkbook' in request.session)) and (request.POST.get('markbook') <> "") :
    #     if not 'markbook' in request.POST:
    #         markbook = Markbook.objects.get(id = request.session['openedMarkbook'])
    #         MarkbookChoice = MarkbookForm({'markbook': markbook})
    #     else:
    #         MarkbookChoice = MarkbookForm(request.POST)
    #         request.session['openedMarkbook'] = request.POST.get('markbook')
    #         markbook = Markbook.objects.get(id = request.POST.get('markbook'))
        students = Student.objects.filter(schForm = markbook.form)
        students = sorted(students, key = attrgetter('user.last_name', 'user.first_name'))
        lessons = Lesson.objects.filter(markbook = markbook)
        lessons = sorted(lessons, key = attrgetter('date', 'num'))
        n = 1 #Для нумерации учеников в списке
        result = {} #Список словарей (содержит все данные журнала)
        dates = [] #Список дат всех уроков (сколько дат -- столько уроков)
        types = []
        q = 0 #Количество уроков
        months = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] #Список, содержащий информацию и количестве уроков в каждом месяце
        m = 1 #Месяц на проверку (изначально -- январь)
        c = 0
        works_num_com = 0
        for lesson in lessons:
            works = Work.objects.filter(lesson = lesson)
            works_num = works.count()
            for work in works:
                type = [work.type.titleSh, str(work.type.weight), 0]
                types.append(type)
            if works_num == 0:
                works_num = 1
                types.append(["", "", 0])
            types[works_num_com][2] = 1
            works_num_com += works_num
            date_cor = (lesson.date.day, works_num, lesson.id)
            dates.append(date_cor)
            q = q + 1
            # hometasks = Hometask.objects.filter(lesson = lesson)
            works = Work.objects.filter(lesson = lesson)
            b = True
            for work in works:
                c += 1
                b = False
            if b:
                c += 1
            while int(lesson.date.month) > m:
                m = m + 1
            months[m-1] = months[m-1] + works_num
        result = OrderedDict()
        #results = []
        for student in students:
            name = str(n) + ". " + student.user.last_name + " " + student.user.first_name
            n = n + 1
            row = []
            #rowForResults = [student.id, name]
            sum = 0
            w = 0
            for lesson in lessons:
                works = Work.objects.filter(lesson = lesson)
                a = True
                for work in works:
                    a = False
                    WorkMarks = WorkMark.objects.filter(work = work, student = student)
                    mks = ""
                    b = True
                    for wm in WorkMarks:
                        if wm.mark is not None:
                            mks = mks + unicode(wm.mark) + " "
                            # row.append(unicode(hm.mark))
                            sum = sum + wm.mark * wm.work.type.weight
                            w = w + wm.work.type.weight
                            b = False
                    row.append(mks)

                if a:
                    row.append("")
            if w > 0:
                average = str(format(sum / w, '.2f'))
            else:
                average = ""
            row.append(average)
            #rowForResults.append(average)
            resultMarks = Result.objects.filter(markbook = markbook, student = student)
            resultMarkStr = ""
            row.append("")
            for resultMark in resultMarks:
                resultMarkStr += unicode(resultMark.mark)
            row.append(unicode(resultMarkStr))
            #rowForResults.append(unicode(resultMarkStr))
            result[name] = row
            #results.append(rowForResults)

        rated = OrderedDict(sorted(result.iteritems(), key = lambda st: st[1][c], reverse = True))
        i = 1
        last = "-"
        ratings = []
        for name in rated:
            if result[name][c] != "":
                if last == result[name][c]:
                    i += 1
                else:
                    ratings.append(i)
                    i = 1
                last = result[name][c]
            else:
                break
        ratings.append(i)
        i = 1
        a = 1
        n = 0
        b = 0
        ratings_ = ratings[:]
        for name in rated:
            if result[name][c] <> "":
                if b == 0:
                    b = ratings[i]
                if a == b:
                    strRating = str(a)
                else:
                    strRating = str(a) + "-" + str(b)
                if ratings_[i] > 0:
                    result[name][c+1] = strRating
                    ratings_[i] -= 1
                    n += 1
                else:
                    i += 1
                    a = b + 1
                    b = b + ratings[i]
                    ratings_[i] -= 1
                    #a = n
                    if a == b:
                        strRating = str(a)
                    else:
                        strRating = str(a) + "-" + str(b)
                    result[name][c+1] = strRating
                    n += 1
            else:
                result[name][c+1] = ""
        #rated = sorted(announcements, key = lambda announcement: announcement.date_pub, reverse = True)
        names = list(result.keys())
        names.sort()


        return render(request, "marks/index.html", {"title": "Классный журнал",
                                                    "MarkbookChoice": MarkbookChoice,
                                                    "students": students,
                                                    "dates": dates, "q": q,
                                                    "months": months, "result": result,
                                                    "types": types, "d": markbookId})
    # else:
    #     MarkbookChoice = MarkbookForm()
    #     try:
    #         del request.session['openedMarkbook']
    #     except:
    #         pass
    #     return render(request, "marks/index.html", {"title": "Классный журнал", "MarkbookChoice": MarkbookChoice, "error": True})

@login_required
def results(request, markbookId):
    markbook = Markbook.objects.get(id = markbookId)
    students = Student.objects.filter(schForm = markbook.form)
    students = sorted(students, key = attrgetter('user.last_name', 'user.first_name'))
    resultsT = []
    n = 1
    for student in students:
        row = [str(student.id), str(n) + ". " + student.user.last_name + " " + student.user.first_name]
        # results = Result.objects.get(markbook__id = markbook.id, student__id = student.id)
        try:
            results = Result.objects.get(markbook__id = markbook.id, student__id = student.id)
            resultForRow = str(results.mark)
        except:
            resultForRow = ""
        # resultForRow = ""
        # for result in results:
        #     resultForRow += str(result.mark)
        row.append(resultForRow)
        resultsT.append(row)
        n += 1
    return render(request, "marks/results.html", {"title": "Итоговый оценки",
                                                    "results": resultsT,
                                                    "markbook": markbook.id})
@login_required
def editingResults(request):
    if request.method == 'POST':
        markbook = Markbook.objects.get(id = request.POST.get('markbook'))
        students = Student.objects.filter(schForm = markbook.form)
        for student in students:
            result = Result.objects.update_or_create(markbook__id = markbook.id, student__id = student.id, defaults = {'markbook': markbook, 'student': student, 'mark': request.POST.get(str(student.id))})
    return HttpResponseRedirect("/markbook/")

@login_required
def editLesson(request, markbookId, lessonId):
    lesson = Lesson.objects.get(id = int(lessonId))
    markbook = Markbook.objects.get(id = markbookId)
    students = Student.objects.filter(schForm = markbook.form)
    students = sorted(students, key = attrgetter('user.last_name', 'user.first_name'))
    cols = Work.objects.filter(lesson__id = lessonId)
    if request.method == 'POST':
        lesson.topic = request.POST['topic']
        lesson.hometask = request.POST['hometask']
        # lesson.num = request.POST['num']
        lesson.save()
        for student in students:
            absence = Absence.objects.filter(student = student, lesson = lesson).delete()
            if request.POST[str(student.id)] <> '':
                absence = Absence.objects.create(lesson = lesson, student = student, value = request.POST[str(student.id)])
            for col in cols:
                WorkMark.objects.filter(work = col, student = student).delete()
                try:
                    marks = request.POST[str(col.id) + "_" + str(student.id)].split('/')
                    for mark in marks:
                        if mark <> '':
                            new = WorkMark.objects.create(work = col, student = student, mark = int(mark))
                except:
                    try:
                        Work.objects.get(id = col.id).delete()
                    except:
                        pass
        newWorks = request.POST['count'][:-2].split('__')
        i = 0
        if newWorks <> ['']:
            for each in newWorks:
                type = WorkType.objects.get(id = int(each))
                work = Work.objects.create(type = type, lesson = lesson)
                for student in students:
                    try:
                        marks = request.POST["new_" + str(i) + "_" + str(student.id)].split('/')
                        for mark in marks:
                            if mark <> '':
                                new = WorkMark.objects.create(work = work, student = student, mark = int(mark))
                    except:
                        try:
                            work.delete()
                        except:
                            pass
                i += 1
        return HttpResponseRedirect("/markbook/")
    else:
        editLessonForm = LessonForm({'topic': lesson.topic, 'hometask': lesson.hometask})

        grand = []

        n = 1
        for student in students:
            name = str(n) + '. ' + student.user.last_name + ' ' + student.user.first_name
            n += 1
            try:
                absence = (str(student.id), Absence.objects.get(student = student, lesson = lesson).value)
            except:
                absence = (str(student.id), '')
            marks = []
            for col in cols:
                mark = ''
                marks_ = WorkMark.objects.filter(work__id = col.id, student = student)
                for each in marks_:
                    mark += str(each.mark) + "/"
                if mark <> '':
                    if mark[-1] == "/":
                        mark = mark[:-1]#Как я это сделал??
                marks.append((str(col.id) + "_" + str(student.id), str(mark)))
            grand.append((name, absence, marks))
        types = WorkType.objects.all()
        return render(request, "marks/edit.html", {"title": "Выставление оценок",
                                                    "editLessonForm": editLessonForm,
                                                    "grand": grand, "works": cols, "types": types})
